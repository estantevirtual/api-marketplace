# api-doc
Documentação do serviço API Marketplace para integração na Estante Virtual


### Sobre a API

O serviço de API da Estante Virtual fornece uma forma alternativa de criar, atualizar e remover produtos na EV, além de também receber e enviar informações de pedidos.


### Modo de usar

Todos os métodos da nossa API esperam um token de autenticação, válido por 24h. Para obtê-lo, é necessário seguir o passo abaixo:

```curl -X POST --data “email=email&password=senha” https://api-marketplace.estantevirtual.com.br/auth/login```

O retorno deve ser um hash com o valor do auth_token. Exemplo:

`{“auth_token”:“eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1MDc4MjYzMTd9.6jV-bb3C5oV-0e4CcJ212Yr1DfROgBlovkFdisc8tC0”}`


O envio do token de autenticação deverá ser enviado como um header de autorização do tipo Bearer Token. Exemplo:

```Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWxsZXJfaWQiOjIzLCJleHAiOjE1OTQyMzA3NDZ9.pWN0GXtO3RDGsRIxX6imi3IFCG6654UbRDYxIDDzd6o```

##### Campo id_vendedor

- O campo id_vendedor que é obrigatório em todos os requests será fornecido ao vendedor quando ele for integrar. Ele é a identificação desse vendedor no site da EV e nunca irá mudar.


### Endpoints

- Autenticação (POST):
`https://api-marketplace.estantevirtual.com.br/auth/login`

- Criação de Produto (POST):
`https://api-marketplace.estantevirtual.com.br/produtos`

- Atualização de Produto (PUT):
`https://api-marketplace.estantevirtual.com.br/produtos`

- Atualização de Estoque (PUT):
`https://api-marketplace.estantevirtual.com.br/estoques`

- Atualização de Preço (PUT):
`https://api-marketplace.estantevirtual.com.br/precos`

- Atualização de status do pedido (PUT):
`https://api-marketplace.estantevirtual.com.br/pedido`

- Geração de etiquetas para pedidos MLE:
`https://api-marketplace.estantevirtual.com.br/etiquetas`

Além desses pontos, usamos endpoints do lado dos vendedores para que possamos fazer `POST` de pedidos e `PUT` de status de cada pedido.


##### Observação importante caso um vendedor já venda na estante e irá integrar pela API:

- A API só trabalha com os livros que foram cadastrados pela API. Ou seja, como o livreiro usa outro método (cadastro manual ou planilha) para os cadastros desses livros a API não vai editar e quando acontecer uma venda desse livro ela vai avisar de uma venda sem o código. Logo, o vendedor não vai saber qual livro foi vendido. O processo que fazemos então ao iniciar uma nova integração é apagar o acervo dele na Estante. E depois o integrador precisa enviar os resquests de cadastro (POST) de todos os livros desse livreiro que ele tem. A partir daí, podemos trabalhar com a edição deles pela API de forma normal. Mas atenção, existem vendas por boleto que podem estar ocorrendo, e precisarão ser tratadas pelo painel somente nesse momento inicial.



### Envio de produtos
_Endpoints responsáveis por cadastrar e atualizar produtos na EV. Ambos tem o mesmo formato mas, caso queira criar um produto, esse request precisa ser enviado como POST, caso queira somente atualizar um produto mandá-lo como PUT._

##### POST ou PUT /produtos (Vendedor -> EV):

```
{
  "produtos": [
    {
      "categoria": "critica literaria",
      "categoria_id": 50,
      "autores": ["Autor 1","Aautor 2"],
      "codigo": "123456",
      "id_vendedor": 2124720,
      "marca": "livro_teste",
      "nome": "Meu livro de teste",
      "skus": [
        {
          "nome": "Meu livro de teste",
          "codigo": "123456",
          "marca": "livro_teste",
          "descricao": "Descrição teste para o produto teste Livro",
          "isbn": "9788506082096",
          "altura": "20.00",
          "comprimento": "20.00",
          "largura":"25.00",
          "peso":"200",
          "idioma": "Pt",
          "imagens":["https:\/\/images-na.ssl-images-amazon.com\/images\/I\/91xWp9kdIDL._SX355_.jpg"],
          "status": "Ativo",
          "estoque": "1",
          "valor_de": "17.0",
          "valor_por": "7.0",
          "prazo_postagem": "2",
          "ano": 2018
        }
      ]
    }
  ]
}
```

_Caso o estoque de um determinado produto esteja zerado, basta [atualizar o estoque](https://gitlab.com/estantevirtual/api-marketplace/#atualizar-estoque), não sendo necessária a criação de um novo produto._ 

##### IMPORTANTE: 

- Uma vez criado o produto, ele deve ser modificado usando PUT e não POST (nunca alterando o atributo **tipo**). POSTs feitos para um produto já criado serão ignorados, não modificando o produto existente nem criando outro. 
- O campo **tipo** não deve ser alterado. Caso um SKU seja cadastrado como "NOVO", ele não deve ser alterado para "USADO" e vice-versa. Esse tipo de alteração ocasionará a duplicidade de registros.
- O campo **tipo** representa se o livro é "NOVO" (sem uso algum) ou "USADO" (com algum tipo de uso). Esse campo não é obrigatório. No entanto, caso não seja enviado, o sistema irá considerar com o valor padrão "NOVO". Informar corretamente o tipo é importante para garantir que os usuários da API recebam dados precisos sobre o estado de cada livro, ajudando na transparência e na qualidade das informações disponibilizadas.
- Pode acontecer de retornarmos o erro 422 ao atualizar um livro, contendo essa mensagem "Um ou mais livros não puderam ser atualizados, porque ainda não foram cadastrados na nossa base. Utiliza o endpoint de cadastro primeiro". Para resolvê-lo utilize o endpoint de cadastro (POST) primeiro (apenas com os livros que não foram atualizados). Esse erro ocorre pois mandaram um PUT num livro que é inexistente na nossa base.
- o campo 'prazo_postagem' deve conter o prazo de postagem para livros sob encomenda e não é de preenchimento obrigatório.
- Cada array deve conter apenas um produto. Os produtos devem ser enviados de um em um.
- Os campos **valor_de** e **valor_por** são usados para alteração de preço. O **valor_por** é o preço que será exibido o produto no site, e o **valor_de** seria um valor antigo (caso exista), como se fosse um histórico de preço. Caso queria mais informações para atualizar preços [clique aqui](https://gitlab.com/estantevirtual/api-marketplace/#atualizar-preços).
- O campo **estoque** não tem limite de valor, porém na nossa base existe um limite para exibição e para cadastro desses produtos. O valor padrão são cinco exemplares por livreiro, podendo variar de valor. Por exemplo: Caso o request seja enviado como **estoque=200**, e o limite desse vendedor seja de cinco exemplares. Nós recebemos o request e cadastraremos apenas 5 exemplares desse produto e exibiremos os cinco até que eles sejam vendidos.
- O campo **codigo** representa o identificador único do vendedor que utiliza a API e deve ser exclusivo para cada produto. Essa exclusividade é crucial para garantir que não haja duplicidade de informações, facilitando o gerenciamento e o rastreamento de cada produto de forma individual. A singularidade do **codigo** permite um controle eficaz dos produtos, assegurando que cada um possa ser identificado, consultado e atualizado sem risco de conflito com outros registros.

Campos: 

- categoria: [nome da Estante utilizada na Estante Virtual](https://www.estantevirtual.com.br/conteudo/lista-estantes)
- **categoria_id**: [ID da categoria na Estante Virtual](https://gitlab.com/estantevirtual/api-marketplace/blob/master/README.md#categoria_id-de-para-id-das-categorias-na-ev)
- **autores**: autor ou autores do livro
- **codigo**: identificador do produto no vendedor
- **id_vendedor**: [ID do vendedor na Estante Virtual](https://gitlab.com/estantevirtual/api-marketplace/#campo-id_vendedor)
- **marca**: editora do livro
- **nome**: título do livro
- **descricao**: sinopse.
- **isbn**: isbn do livro
- altura: altura do exemplar em centímetros
- comprimento: comprimento do exemplar em centímetros
- largura: largura do exemplar em centímetros
- **peso**: peso do exemplar em gramas
- idioma: código do idioma do exemplar, de acordo com listagem abaixo.
- imagens: url da imagem de capa
- status: status do exemplar
- **estoque**: quantidade de exemplares disponíveis
- **valor_de**: preço antigo
- **valor_por**: preço atual (Caso exista somente um preço, basta colocar o mesmo nos dois campos) 
- **ano**: ano de lançamento da edição do exemplar
- tipo: Indica a condição do livro, sendo "NOVO" para livros sem uso e "USADO" para livros com uso.

_Campos em negrito são de preenchimento obrigatório._ 

###### categoria_id - Id da categoria na EV:

```
|  1 | Administração          |
|  2 | Antropologia           |
|  3 | Arqueologia            |
|  4 | Arquitetura            |
|  5 | Astronomia             |
|  6 | Biologia               |
|  7 | Ciência Política       |
|  8 | Ciências Exatas        |
|  9 | Comunicação            |
| 10 | Contabilidade          |
| 11 | Direito                |
| 12 | Ecologia               |
| 13 | Economia               |
| 14 | Filosofia              |
| 15 | História do Brasil     |
| 16 | História Geral         |
| 17 | Geografia              |
| 18 | Informática            |
| 19 | Engenharia             |
| 20 | Linguística            |
| 22 | Medicina               |
| 23 | Música                 |
| 24 | Pedagogia              |
| 25 | Psicologia             |
| 26 | Sociologia             |
| 27 | Artes                  |
| 28 | Brasil                 |
| 29 | Auto Ajuda             |
| 30 | Biografias             |
| 31 | Cinema                 |
| 32 | Culinária              |
| 33 | Decoração              |
| 34 | Didáticos              |
| 35 | Botânica               |
| 36 | Hobbies                |
| 37 | Moda                   |
| 38 | Jogos                  |
| 39 | Comportamento          |
| 40 | Ensino de Idiomas      |
| 41 | Esoterismo             |
| 42 | Folclore               |
| 43 | Guerra                 |
| 44 | Esportes               |
| 45 | Ficção Científica      |
| 46 | Genealogia             |
| 47 | Infanto Juvenis        |
| 48 | Literatura Brasileira  |
| 49 | Literatura Estrangeira |
| 50 | Crítica Literária      |
| 51 | Poesia                 |
| 52 | Contos                 |
| 54 | Saúde                  |
| 55 | Sexualidade            |
| 56 | Teatro                 |
| 57 | Fotografia             |
| 58 | Pintura                |
| 59 | Turismo                |
| 60 | Humor                  |
| 61 | Religião               |
| 62 | Artesanato             |
| 63 | Pecuária               |
| 64 | Agricultura            |
| 65 | Outros Assuntos        |
| 66 | Revistas               |
| 67 | Jornais                |
| 68 | Coleções               |
| 69 | Dicionários            |
| 70 | Enciclopédias          |
| 71 | Gibis                  |
| 72 | Livros Raros           |
| 73 | Documentos             |
| 74 | Manuscritos            |
```


###### Códigos de idiomas aceitos:

- "De" = Alemão
- "Ch" = Chinês
- "Co" = Coreano
- "Da" = Dinamarquês
- "Es" = Espanhol
- "Fr" = Francês
- "El" = Grego
- "He" = Hebraico
- "Ho" = Holandês
- "Hu" = Húngaro
- "En" = Inglês
- "It" = Italiano
- "Jp" = Japonês
- "La" = Latim
- "Pl" = Polonês
- "Pt" = Português
- "Ru" = Russo

_Por padrão, esse campo é definido como "Pt", caso não seja informado pelo vendedor._


### Atualizar preços
_Endpoint responsável pela atualização de preços de um produto. Ele atualiza o preço de todos os exemplares de um produto desse vendedor que tenham sido cadastrados com o código fornecido. Caso o produto não exista, retorna um erro._

##### PUT /precos (Vendedor -> EV):

```
{
  "precos":
    [
       {
           "codigo": "PRODNORMALCOD1",
           "valor_de": 892.18,
           "valor_por": 743.48,
           "id_vendedor":3066499
       },
       {
           "codigo": "PRODNORMALCOD2",
           "valor_de": 100.18,
           "valor_por": 98.48,
           "id_vendedor":3066499
       },
    ]
}
```
Retorno:
```
{
   "success": 201
}
```
OU
```
{
   "error": 404,
   "message" : "Produto inválido."
}
```

### Caso deseje excluir um produto da nossa base
_Caso deseje que todos os exemplares de um mesmo produto sejam removidos da nossa base, basta mandar um chamado para o endpoint de atualização de estoque com o estoque igual a 0. Mais informações de atualização de estoque logo abaixo._

### Atualizar estoque
_Endpoint responsável pela atualização de estoque de um produto. Ele verifica se o estoque é maior ou menor do que consta na nossa base. Se a quantidade de estoque for maior, ele irá aumentar esse estoque até o estoque desse vendedor estar igual ao enviado pelo request. Por exemplo: Temos 7 exemplares no acervo desse vendedor e no request veio o campo **estoque = 10**, então adicionamos mais 3 exemplares na nossa base. Já se o estoque for menor ele exclui os exemplares até o acervo desse livreiro estar igual ao do request. Caso o produto não exista, retorna um erro._ 


_Caso o estoque de um determinado produto esteja zerado, basta atualizar o estoque, não sendo necessária a criação de um novo produto._ 

##### PUT /estoques (Vendedor -> EV):

```
{
  "estoques":
    [
       {
            "codigo": "PRODNORMALCOD1",
            "estoque": 144,
            "id_vendedor":3066499
       },
       {
            "codigo": "PRODNORMALCOD2",
            "estoque": 0,
            "id_vendedor":3066499
       }
    ]
}
```

Retorno:
```
{
   "success": 201
}
```
OU

```
{
   "error": 404,
   "message" : "Produto inválido."
}
```


### Enviar pedidos
_De 10 em 10 minutos nós procuramos na nossa base se existem pedidos novos que não foram mandados para o vendedor. Ou pedidos que tentamos enviar mas não tivemos sucesso por algum erro. Para cada pedido será enviado um request. A quantidade de produtos do pedido pode variar, mas sempre será um pedido por vez._

###### Observação:

- o campo `telefone1` é passado como string e não como um formato de número de telefone. E caso o cliente possua mais de um telefone só será enviado o primeiro. 

##### POST /pedido (EV -> Vendedor):

```
{ 
   "id_vendedor":340690,
   "pedidos":[ 
      { 
         "id_cliente":3030537,
         "transportadora":"normal",
         "marketplace_id_pedido":27596692,
         "marketplace_id_pedido_loja": 600382031,     # Somente será exibido se a origem for o novo marketplace
         "marketplace_store":"estantevirtual",
         "parcelas":1,
         "status_codigo":"PROCESSING",                # status passados: EXPIRED | PROCESSING | CANCELED | STANDBY | SENT
         "marketplace_data_pedido": '2018-04-25 00:00:00',
         "data_estimada_entrega": '2018-05-06 00:00:00',
         "valor_total_pedido":"38.42",
         "valor_total_frete":9.42,
         "forma_pagamento":"Cartão de crédito",        # Poderá ser enviado como: Cartão de crédito, Boleto ou Pix
         "id_pagamento":"ORD-BIGLMMLS8HAV",            # Contém o ID que o Gateway usa para identificar o pagamento
         "valor_total_desconto":0,
         "cliente":{ 
            "tipo":"PF",
            "razao_social":"Ana Silva",
            "cpf_cnpj":"05773931702",
            "email":"asilva@google.com.br",
            "data_aniversario":nil,
            "telefone1":"21 23456781"
         },
         "endereco":{ 
            "tipo_endereco":"Entrega",
            "nome_destinatario":"Ana Silva",
            "estado":"Rio de Janeiro",
            "cidade":"Rio de Janeiro",
            "bairro":"Centro",
            "logradouro":"Rua Maricá",
            "numero":"0",
            "complemento":"ap 101",
            "cep":"22220-010"
         },
         "produtos":[ 
            { 
               "codigo":"1670133",
               "quantidade":1,
               "valor":"29.0",
               "desconto":0
            }
         ]
      }
   ]
}
``` 
 
Retorno:

```
{
   "success": 201,
   "id_pedido": 20
}
```

###### Observação:

- Sempre fazemos a cotação do frete nos correios, não existe opção de selecionar formas de envio pela API. Então, no campo transportadora sempre enviamos para vocês uma opção dos correios que foi escolhida pelo cliente na hora de comprar na Estante Virtual. São elas: Impresso normal, PAC ou Sedex.
- Os status que poderemos passar e o que eles representam:
   - EXPIRED: Pedido expirado.
   - PROCESSING: O pagamento do pedido está sendo processado. 
   - CANCELED: O pedido foi cancelado.
   - STANDBY: O pedido está aguardando para ser enviado.
   - SENT: Pedido enviado. Esse aqui cabe ao vendedor nos enviar o request contendo esse status. Nós só enviamos o status de volta, porque toda a mudança de status nós mandamos para o vendedor.


### Atualizar pedido
_O vendedor pode mandar um request para a EV, ou vice-versa, com a atualização de um pedido. Caso seja um request para a EV e não ocorra erro no processo atualizaremos o pedido com as informações que estão no request. Caso o produto não exista, retorna um erro._



##### PUT /pedido (Vendedor -> EV)

```
{
  "pedido":
    {
    "id_vendedor": 2124720,
    "marketplace_id_pedido": 101,
    "status_codigo" : "[STATUS_DO_PEDIDO]",                    # status aceitos: NEW | STANDBY | CANCELED | SENT
    "razao_cancelamento" : "Campo preenchido se necessário",   # deve ser preenchido no envio do status CANCELED
    "rastreio" : "codigo de rastreio correios",                # deve ser preenchido no envio do status SENT, quando a entrega for via Correios 
    "url_rastreio":"url da transportadora"                     # deve ser preenchido  no envio do status SENT, quando a entrega for via transportadora 

    }
}
```

Retorno:

```
{
   "success": 200,
}
```

OU

```{
   "error": 400,
   "message" : "Status inválido."
}
```

###### Observações:

- Caso seja enviado o status CANCELED, o campo razao_cancelamento deverá ser preenchido com um dos motivos abaixo ['sem_estoque', 'erro_processamento', 'cliente_devolveu'].

- Caso seja enviado o status SENT, o campo **rastreio** ou **url_rastreio** deverão ser preenchidos. Importante: **Não é permitido preencher os dois campos ao mesmo tempo** (neste caso, irá ocorrer um erro de status 500 e um texto contendo {\"error\":\"Blank Code\"}"). É um ou o outro. O vendedor pode enviar pelos correios conforme selecionado pelo cliente, ou por uma transportadora. Essa escolha fica a cargo do vendedor, e cabe a ele lidar com qualquer custo extra que isso acarrete.
   - Se o pedido for enviado pelos correios, preencher o código de rastreamento no campo "rastreio".
   - Se o pedido for enviado por uma transportadora, preencher a URL de rastreamento no campo "url_rastreio".


##### PUT /pedido (EV -> Vendedor)

```
{
    "id_vendedor": 2497356,
    "pedidos": [
        {
            "marketplace_id_pedido": 27598273,
            "status_codigo": "PROCESSING",
            "data_estimada_entrega": ""
        }
    ]
}
```

###### Observações:

- Identificar e ignorar um POST repetido de um pedido, ou seja, ignorar quando um pedido que já foi recebido por vocês for enviado novamente como um novo pedido. O mesmo vale para um PUT igual.

- Outra ação é não aceitar um PUT de um pedido que não existe um POST criado. Ou seja, vocês podem receber uma atualização de um pedido que a criação ainda não foi enviada para o vendedor. Isso pode acontecer devido a não sincronização dos processos, mas quando o POST for mandado para o vendedor ele irá com o status mais recente.

- Essas são as transições de status possíveis. Vocês podem, do lado de vocês, implementar a mesma regra e não aceitar caso chegue alguma transição diferente dessas:

```
processing -> standby
standby -> sent
standby -> canceled
sent -> canceled
canceled -> standby
sent -> standby
```

### Gerar etiquetas para pedidos com Magalu Entregas
_O vendedor pode fazer uma requisição para a Estante Virtual, solicitando as etiquetas **de no máximo 25 pedidos com a forma de envio Magalu Entregas (MLE)**, passando via paramêtro de URL os ids dos pedidos. Caso todos os pedidos utilizem a forma de envio MLE, será retornado o status 200 e o binário do arquivo PDF, com a lista de postagem dos pedidos que tiveram etiquetas geradas no mesmo lote e as etiquetas de cada pedido enviado na solicitação._

##### GET /etiquetas?ids=123,456,789,098
**Com sucesso**
Segue abaixo um exemplo com as 10 primeiras linhas do retorno esperado com status 200 contendo o arquivo PDF em binário.

```
%PDF-1.3
1 0 obj
<<
/Type /Pages
/Count 11
/Kids [ 3 0 R 4 0 R 5 0 R 6 0 R 7 0 R 8 0 R 9 0 R 10 0 R 11 0 R 12 0 R 13 0 R ]
>>
endobj
2 0 obj
<<
...
```

**Cenários mistos**
- Resposta com status 207 e XML listando pedidos:
   - que tem etiquetas disponíveis (200)
   - com etiquetas não permitidas (422)
   - com etiquetas indisponíveis (422)
   - não encontrada (404)

exemplo de XML:
```xml
<?xml version="1.0" encoding="utf-8" ?>
<d:multistatus xmlns:d="DAV:">
  <d:response>
    <d:href>https://api-marketplace.estantevirtual.com.br/etiquetas?ids=123</d:href>
    <d:status>HTTP/1.1 200 Etiquetas disponíveis</d:status>
  </d:response>
  <d:response>
    <d:href>https://api-marketplace.estantevirtual.com.br/etiquetas?ids=456</d:href>
    <d:status>HTTP/1.1 404 Etiquetas não encontradas</d:status>
  </d:response>
  <d:response>
    <d:href>https://api-marketplace.estantevirtual.com.br/etiquetas?ids=789</d:href>
    <d:status>HTTP/1.1 422 Etiquetas não permitidas</d:status>
  </d:response>
  <d:response>
    <d:href>https://api-marketplace.estantevirtual.com.br/etiquetas?ids=098</d:href>
    <d:status>HTTP/1.1 422 Etiquetas indisponíveis</d:status>
  </d:response>
</d:multistatus>
```

**Cenários de erros com status 400:**
- Não seja informando o parâmtro ids com pelo menos um id de pedido
- Não seja informado o token de autenticação
- Sejam informados mais de 25 pedidos com MLE por vez


###### Observações:
Abaixo os descritivo para cada um dos tipos de resposta possível:

- Etiquetas disponíveis (status 200): Somente para pedidos com pagamento já aprovado feitos com forma de envio MLE e que ainda não foram enviados, entregues ou cancelados.
- Etiquetas não permitidas (status 422): Para pedidos que **não** foram feitos com a forma de envio MLE.
- Etiquetas indisponíveis (status: 422): Para pedidos com a forma de envio MLE e que **não** estão no status standby.
- Não encontrada (status 404): Ocorre em pedidos que **não** pertencem ao vendedor que fez a requisição.
