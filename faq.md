Esta lista de dúvidas foi enviada pelos desenvolvedores da Anymarket, e incluímos as respostas aqui porque pode ajudar futuros desenvolvedores que forem usar nossa API Marketplace.


# Título dos Livros

## Qual a estrutura recomendada? 

Não temos uma recomendação, cada vendedor cadastra da sua forma. Mas o mais comum é o cadastro ser feito exatamente da forma como é impressa na capa.

## Quantidade máxima de caracteres?

130 caracteres.

## Restrições?

Não usar caracteres de outros idiomas (chinês, etc) e emojis.


# Fotos

## Resolução mínima e máxima?

A regra que temos na nossa documentação interna só informa para que o limite não passe de 5MB. Nós redimensionamos a imagem pra garantir que nada vai quebrar.

## Quantidade mínima e máxima?

Somente 1 foto por produto.

## Restrições?

Não ultrapassar 5MB.

## Pode publicar imagens quadradas e retangulares?

Sim.


# Descrição

## Quantidade máxima caracteres?

65.535 caracteres.

## Restrições?

Não usar caracteres de outros idiomas (chinês, etc) e emojis.

## Aceita HTML ou TEXTO?

Não aceitamos HTML, somente texto.


# SKU

## Quantidade máxima de caracteres no SKU?

Pode ver a resposta sobre limite de caracteres em cada campo.

## Caracteres especiais no SKU?

Não usar caracteres de outros idiomas (chinês, etc) e emojis.


# Código de Barras

## EAN?

EAN não é um dos campos que aceitamos. Mas temos o ISBN, e é obrigatório.


# Outros

## Existe preço máximo?

Não temos uma regra de negócio limitando o máximo do preço, mas somos limitados pelo tamanho que o nosso banco de dados aceita. Atualmente o máximo que nosso banco aceita é um campo decimal(7,2). Isso significa 5 dígitos antes da vírgula e 2 depois. Então o preço máximo acaba sendo, no momento, R$99.999,99.

## Tempo médio de catalogação?

Não demora mais do que alguns poucos minutos para o livro ser catalogado.

## Tem Buybox?

Sim. Menor preço é exibido na Buybox.

## Possui Múltiplas Contas?

Cada vendedor pode ter apenas 1 única conta.

## Tem limite quanto a faixa de desconto?

A única regra é que o preço não pode ser abaixo de R$6 (valor pode mudar no futuro).

### Há algum valor de desconto no valor total do pedido? 

Pela API nós não trabalhamos com desconto. O vendedor pode dar um “desconto” abaixando o valor do livro por meio um request de atualização de preço, favor verificar o item “Atualizar preços” na documentação.

## Precisa vincular categorias?

Sim. Temos lista de categorias na documentação.

## Precisa configurar variações?

Não temos a opção de configurar variações dos livros. Cadastramos da forma que o livro foi enviado.

## De quanto tempo é atualizado o estoque?

Até 5 minutos depois do request ter sido enviado para a nossa API.

## De quanto tempo é atualizado o pedido?

Até 5 minutos depois do request ter sido enviado para a nossa API.

## Existe limite de produtos enviados por dia?

Até o momento, não.

## Qual o Rate Limit para consulta de frete?

Nossa API não tem um endpoint para consulta de frete. O frete é calculado e exibido para o comprador na hora de finalizar a compra.

## É obrigatório o envio de EAN nos produtos?

EAN não é um dos campos da API, favor verificar documentação. ISBN é obrigatório.

## Permite publicar produtos sem estoque?

Se o vendedor não tem o item em estoque, mas quer vender, não pode informar estoque 0. Todo item nosso com estoque 0 é removido do site.

Se o vendedor não tem o produto em estoque e precisa de um prazo maior de entrega por causa disso, é possível enviar um valor alto de estoque (pra garantir) e usar o campo prazo_postagem para informar quantos dias o vendedor vai levar para conseguir postar aquele livro. O prazo máximo é de 20 dias.

## No pedido, vocês informam os dados do cliente? Quais são estes dados?

Sim. Estas informações estão na documentação da API, disponibilizada no Gitlab.

## No pedido, vocês informam a forma de pagamento?

Não.

## Exige DE/PARA de marcas?

Não. O que informarem no campo de marca é o que vai ser exibido no site.

## O Seller pode criar um produto direto no painel do Marketplace e depois vincular com um produto aqui no Hub? Se sim, é baseado pelo SKU?

Vendedores que integram pela nossa API só podem criar produtos pela API, porque alguns dados necessários para essa integração não existem nos livros criados pelo painel. 

Mas é possível acessar o painel para verificar informações de produtos e vendas.

## O marketplace permite o ANYMARKET cancelar a venda?

Sim. Temos um endpoint para editar dados de uma venda, e é possível usar isso para cancelamento. Ver documentação.

## É possível enviar os dados de rastreamento para o Marketplace? Se sim, pode detalhar por favor?

Sim, vocês enviam código ou link de rastreamento (caso não seja pelos correios). Isso é feito pelo endpoint que permite editar dados de uma venda. Ver documentação da API.

## É possível enviar os dados da nota fiscal para o marketplace? Se sim, pode detalhar por favor?

Não temos essa opção.

## Pode ter mais de um SKU no array?

No momento, não.

## O peso está em qual unidade de medidas?

Gramas.

## Trabalha com variações?

Não.

## O Status do produto pode ser alterado?

Se a pergunta for sobre status do pedido, ele pode sim ser alterado. Depois que é vendido, vocês podem alterar para "cancelado". Depois que está pago, vocês podem marcar como "enviado" e informar o código (ou link) de rastreio.

## Quais campos são obrigatórios na API?

Ver documentação.

## Existe reserva de estoque?

O vendedor pode cadastrar pela API o livro com um valor alto de estoque, se quiser. É o recomendado pra vendedores que trabalham com print on demand, ou outra forma de trabalho que não tenha um número bem definido de estoque.

Nosso site exibe apenas 5 cópias de um livro de uma vez, mesmo que o estoque seja maior que 5. Mas conforme eles são vendidos, novos livros saem do estoque de “reserva” e entram no lugar deles, enquanto o estoque ainda for maior que 5.

## Possui ambiente sandbox?

Sim. Podemos liberar o acesso ao nosso sandbox. Precisamos de um email de vendedor, IP de onde os chamados serão feitos, e uma URL pra onde vamos enviar os avisos de compras.

## Posso alterar o tipo do SKU?

Não. Ao cadastrar um SKU como "NOVO", nunca faça uma atualização alterando para "USADO", e vice-versa. Isso pode ocasionar a duplicação de SKUs na base.